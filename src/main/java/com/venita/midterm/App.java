package com.venita.midterm;

/**
 * Hello world!
 *
 */
public class App {

    static Car car1 = new Car("Honda", "Black", "CIVIC", 0, 0);
    static Car car2 = new Car("Mazda", "White", "Mazda BT-50.", 0, 0);
    static Car car3 = new Car("MG", "Red", "MG ZS", 0, 0);
    static Owner person1 = new Owner("A", "20", "Male");
    static Owner person2 = new Owner("B", "21", "Female");
    static Owner person3 = new Owner("C", "19", "Female");

    public static void main(String[] args) {
        String c1 = person1.getName() + " " + person1.getAge() + " " + person1.getGender() + " " + car1.getBrand() + " " + car1.getColor();
        System.out.println(c1);
        String c2 = person2.getName() + " " + person2.getAge() + " " + person2.getGender() + " " + car2.getBrand() + " " + car2.getColor();
        System.out.println(c2);
        String c3 = person1.getName() + " " + person3.getAge() + " " + person3.getGender() + " " + car3.getBrand() + " " + car3.getColor();
        System.out.println(c3);
    }


}
