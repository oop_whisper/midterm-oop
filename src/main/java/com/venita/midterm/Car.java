package com.venita.midterm;

public class Car {
 
    private String brand;
    private String color;
    private String model;
    private int x;
    private int y;



    public Car(String brand, String color,String model,int x,int y) {
        this.brand = brand;
        this.color = color;
        this.model = model;
        this.x = x;
        this.y = y;

    }
    public String getBrand() {
        return brand;
    }

    public String getColor() {
        return color;
    }

    public String getModel() {
        return model;
    }
    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void up() {
       
        y = y - 1;
        
    }

    public void down() {
        y = y + 1;
    }
        


    public void left() {
        x = x - 1;
        
    }



    public void right() {
        x = x + 1;
       
    }
  
    
}


